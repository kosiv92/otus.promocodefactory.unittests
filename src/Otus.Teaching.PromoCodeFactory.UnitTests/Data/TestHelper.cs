﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Data
{
    public class TestHelper
    {
        private readonly DataContext testContext;
        public TestHelper()
        {
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseInMemoryDatabase(databaseName: "TestDbInMemory");

            var dbContextOptions = builder.Options;
            testContext = new DataContext(dbContextOptions);
            // Delete existing db before creating a new one
            testContext.Database.EnsureDeleted();
            testContext.Database.EnsureCreated();
        }

        public IRepository<Partner> GetPartnerRerository()
        {
            return new EfRepository<Partner>(testContext); 
        }
    }
}
