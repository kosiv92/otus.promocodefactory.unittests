﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Client;
using Namotion.Reflection;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using FluentAssertions.Common;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.UnitTests.Data;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly PartnerPromoCodeLimitRequestBuilder _requestBuilder;
        private readonly PartnerPromoCodeLimitBuilder _partnerPromoCodeBuilder;
        private readonly PartnerBuilder _partnerBuilder;
        private readonly IFixture _mockFixture;
        private readonly Guid _id;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _mockFixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = _mockFixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = new PartnersController(_partnersRepositoryMock.Object);
            _requestBuilder = new PartnerPromoCodeLimitRequestBuilder();
            _partnerPromoCodeBuilder = new PartnerPromoCodeLimitBuilder();
            _partnerBuilder = new PartnerBuilder();
            _id = Guid.NewGuid();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsyncTests_ZeroLimit_ReturnsBadRequest()
        {
            //Arrange            
            var requestWithZeroLimit = _requestBuilder.WithZeroLimit().Build();

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(_id))
                .ReturnsAsync(_partnerBuilder.WithId(_id)
                .Build());

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, requestWithZeroLimit);

            //Assert            
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arrange            
            var _request = _mockFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .OmitAutoProperties()
                .Create();

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(_id))
                .ReturnsAsync(_partnerBuilder.IsNull()
                .Build());

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsyncTests_NotActivePartner_ReturnsBadRequest()
        {
            //Arrange            
            var _request = _mockFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .OmitAutoProperties()
                .Create();

            _partnersRepositoryMock.Setup(r => r.GetByIdAsync(_id))
                .ReturnsAsync(_partnerBuilder.NotActive()
                .Build());

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert
            result
                .Should()
                .BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsyncTests_NewLimitSet_ReturnsZeroIssuedPromocodes()
        {
            //Arrange
            var _request = _mockFixture.Create<SetPartnerPromoCodeLimitRequest>();

            var partnerPromoCodeLimit = _partnerPromoCodeBuilder
                .WithPartnerOwnerId(_id)
                .WithLimit(5)
                .Build();

            var _partner = _partnerBuilder
                .WithId(_id)
                .HasNumberIssuedPromoCodes(10)
                .AddPartnerPromoCodeLimit(partnerPromoCodeLimit)
                .Build();

            _partnersRepositoryMock
                .Setup(r => r.GetByIdAsync(_id))
                .ReturnsAsync(_partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert
            _partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsyncTests_NewLimitSet_OldLimitCanceled()
        {
            //Arrange
            var _request = _mockFixture.Create<SetPartnerPromoCodeLimitRequest>();

            var partnerPromoCodeLimit = _partnerPromoCodeBuilder
                .WithPartnerOwnerId(_id)
                .WithLimit(5)
                .Build();

            var _partner = _partnerBuilder
                .WithId(_id)
                .HasNumberIssuedPromoCodes(10)
                .AddPartnerPromoCodeLimit(partnerPromoCodeLimit)
                .Build();

            _partnersRepositoryMock
                .Setup(r => r.GetByIdAsync(_id))
                .ReturnsAsync(_partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert
            _partner.PartnerLimits
                .FirstOrDefault(l => !l.CancelDate.HasValue).Id
                .Should().NotBe(partnerPromoCodeLimit.Id);

            _partner.PartnerLimits
                .FirstOrDefault(l => l.Id == partnerPromoCodeLimit.Id).CancelDate
                .Should()
                .NotBeNull();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsyncTests_NewLimitSet_LimitSavedInRepository()
        {
            //Arrange
            var testHelper = new TestHelper();
            var inMemoryRepo = testHelper.GetPartnerRerository();                       

            var _request = _mockFixture.Create<SetPartnerPromoCodeLimitRequest>();

            var partnerPromoCodeLimit = _partnerPromoCodeBuilder
                .WithPartnerOwnerId(_id)
                .WithLimit(5)
                .Build();

            var _partner = _partnerBuilder
                .WithId(_id)
                .HasNumberIssuedPromoCodes(10)
                .AddPartnerPromoCodeLimit(partnerPromoCodeLimit)
                .Build();

            await inMemoryRepo.AddAsync(_partner);            

            var _partnersControllerWithInMemoryRepo = new PartnersController(inMemoryRepo);

            //Act
            var result = await _partnersControllerWithInMemoryRepo.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert                        

            var partnerFromInMemoryRepo = await inMemoryRepo.GetByIdAsync(_id);

            var activeLimit = partnerFromInMemoryRepo.PartnerLimits
                .FirstOrDefault(l=>l.CancelDate != null);

            activeLimit.Should().NotBeNull();
            activeLimit.Limit.Should().Equals(_request.Limit);
            activeLimit.EndDate.Should().Equals(_request.EndDate);
        }

        public class PartnerBuilder
        {
            Partner _partner = new Partner();

            public PartnerBuilder()
            {
                _partner.Id = Guid.NewGuid();
                _partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
                _partner.IsActive = true;
            }

            public PartnerBuilder WithId(Guid id)
            {
                _partner.Id = id;
                return this;
            }

            public PartnerBuilder IsNull()
            {
                _partner = null;
                return this;
            }

            public PartnerBuilder NotActive()
            {
                _partner.IsActive = false;
                return this;
            }

            public PartnerBuilder HasNumberIssuedPromoCodes(int number)
            {
                _partner.NumberIssuedPromoCodes = number;
                return this;
            }

            public PartnerBuilder AddPartnerPromoCodeLimit(PartnerPromoCodeLimit partnerPromoCodeLimit)
            {
                _partner.PartnerLimits.Add(partnerPromoCodeLimit);
                return this;
            }

            public Partner Build() => _partner;
        }

        public class PartnerPromoCodeLimitRequestBuilder
        {
            SetPartnerPromoCodeLimitRequest _setPartnerPromoCodeLimitRequest
                = new SetPartnerPromoCodeLimitRequest();

            public PartnerPromoCodeLimitRequestBuilder()
            { }

            public PartnerPromoCodeLimitRequestBuilder WithZeroLimit()
            {
                _setPartnerPromoCodeLimitRequest.Limit = 0;
                return this;
            }

            public SetPartnerPromoCodeLimitRequest Build()
                => _setPartnerPromoCodeLimitRequest;
        }

        public class PartnerPromoCodeLimitBuilder
        {
            PartnerPromoCodeLimit _partnerPromoCodeLimit = new PartnerPromoCodeLimit();

            public PartnerPromoCodeLimitBuilder()
            {
                _partnerPromoCodeLimit.Id = Guid.NewGuid();
                _partnerPromoCodeLimit.CreateDate = DateTime.Now;
                _partnerPromoCodeLimit.EndDate = DateTime.Today.AddDays(5);
            }

            public PartnerPromoCodeLimitBuilder WithPartnerOwnerId(Guid id)
            {
                _partnerPromoCodeLimit.PartnerId = id;
                return this;
            }

            public PartnerPromoCodeLimitBuilder WithLimit(int limit)
            {
                _partnerPromoCodeLimit.Limit = limit;
                return this;
            }

            public PartnerPromoCodeLimit Build()
            {
                return _partnerPromoCodeLimit;
            }

        }
                
    }
}